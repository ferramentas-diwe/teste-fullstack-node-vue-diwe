# Teste para desenvolvedor Fullstack Node.js / Vue.js

Desenvolver uma RESTful API e uma pequena aplicação a partir do estudo de caso proposto

## Premissas
- Utilizar banco de dados relacional
- Utilizar algum framework do Node.js
- Modelagem do banco de dados com o desafio proposto
- Utilizar o Vue.js no frontend

## Diferenciais
- Documentação dos endpoints da API
- Utilizar o Vuetify
- Utilizar o framework Loopback
- Construção de componentes reutilizáveis

## Objetivo
O objetivo do desafio é construir uma API e uma pequena aplicação para gerenciar os CRUD's a partir do modelo proposto, queremos analisar suas capacidades em planejamento, modelagem e organização de componentes.

## O Desafio
João tem em seu sistema uma tabela com as roles do seu sistema (ANEXO 1) e uma tabela de categorias (ANEXO 2), porém elas ainda não estão em uso. 

João precisa listar e criar os relacionamentos de todas as roles e suas respectivas categorias. Ele definiu que como regra de negócio que uma role pode ter várias categorias e cada categoria cadastrada pode pertencer a mais de uma role.

João também deseja gerenciar (CREATE, READ, UPDATE, DELETE) as categorias e roles do seu sistema.

João gosta muito da visão de Kanban e de utilizar drag n drop para manipular quais categorias devem estar em cada uma das roles.

## Pontos de atenção

Vamos analisar sua capacidade de interpretação da situação problema, como vai se comportar e propor soluções.

O design da aplicação não é um fator importante para este teste, mas é claro que vamos levar em consideração.

## O que vamos avaliar?
1. Funcionamento e método de resolução do problema.
2. Organização do código.
3. Performance do código.
4. Documentação da API.
5. Organização dos componentes
6. Semântica, estrutura, legibilidade, manutenibilidade, escalabilidade do seu código e suas tomadas de decisões.
7. Gerenciamento de estado
8. Históricos de commits do git.

## Entrega

Você deve enviar o link do repositório PUBLICO para o endereço de e-mail: 

mateus.silva@diwe.com.br CC: maicon.passos@diwe.com.br

# ANEXO 1 - Tabela de Roles

| id |  RoleName  |  Description  |  Active  |
|--- | ---------- | ---------- | ---------- |
| 1  |  Jogador | Jogador de futebol | true
| 2  |  Colecionador | Colecionador de álbums | true
| 3  |  Cliente | Cliente da banca | false

# ANEXO 2 - Tabela de Categorias

| id |  CategoryName | Active  |
|--- | ---------- | ---------- |
| 1  |  Copa do Mundo | true
| 2  |  Super Herói | true
| 3  |  Futebol | false